<?php
/*	Author: Russel Rodrigues
*	Creates folders in a
*  	directory based on 
*	file extensions   	
*/
$path 	= 'D:\Download';
$ext 	= 'pptx'; 
$folder_name = 'all_ppt';
$a =  scandir($path);

$extarray = array();

if(count($a) > 0):
foreach ($a as $key => $value) {
	$ab = explode('.', $value);
	$match_ext = end($ab);
	if(isset($match_ext) && strtolower($match_ext) == $ext):
		array_push($extarray, $value);
	endif;
}
endif;

if(count($extarray) > 0):
	if(!file_exists($path.'\\'.$folder_name)):
	mkdir($path.'\\'.$folder_name, 0777, true);
	endif;

	foreach ($extarray as $key1 => $value1) {
		if(!file_exists($path.'\\'.$folder_name.'\\'.$value1)):
			rename($path.'\\'.$value1, $path.'\\'.$folder_name.'\\'.$value1);
		endif;
	}
echo 'success';
else:
	echo 'no data to move';
endif;
